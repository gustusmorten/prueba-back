# REST API Prueba Teamknowlogy

Proyecto generado como prueba para Teamknowlogy.


`.env` es el archivo para configurar las variables  de entorno del proyecto.

## Install

    npm install

## Run the app

    npm run dev

## Run the tests

    npm run test

# REST API

## Send an email

### Request

`POST /api/email/`

    curl --location --request POST 'http://localhost:3000/api/email/' --header 'Content-Type: application/json'--data-raw'{"email":"test.@email.com"}'

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Location: /api/email/
    Content-Length: 59

    {"data":"joseluis.ct1997@gmail.com","message":"Email send"}

## Check for mutations and store DNA 

`POST /api/mutation/`

    curl --location --request POST 'http://localhost:3000/api/mutation/' --header 'Content-Type: application/json' --data-raw '{"dna": ["RTYUED","POELAD","RTEPED","LPAFED","RAMNRA","APQSXA"]}'

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Location: /api/mutation/
    Content-Length: 43

    {"data":{"hasMutation":true},"message":""}}

## Get stats of mutations 

`POST /api/stats`

    curl --location --request GET 'http://localhost:3000/api/mutation/stats' --data-raw ''

### Response

    HTTP/1.1 200 OK
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Location: /api/mutation/
    Content-Length: 79

    {"data":{"count_mutations":5,"count_no_mutation":3,"ratio":0.625},"message":""}
