class EmailServiceMock {
    async sendEmailTo(email) {
       return Promise.resolve(true);
    }
}

module.exports = EmailServiceMock;