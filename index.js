const express = require('express');
const { config } = require('./config/index');
const {mutationAPI, mutationStatsAPI} = require('./routes/mutations');
const emailAPI = require('./routes/email');
const bodyParser = require('body-parser');
const mysql = require('mysql');

require('./db');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
mutationAPI(app);
mutationStatsAPI(app);
emailAPI(app);
app.listen(config.port, function() {
  console.log(`Listening http://localhost:${config.port}`);
});