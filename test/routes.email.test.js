const assert = require('assert');
const proxyquire = require('proxyquire');
const testServer = require('../utils/testServer');
const EmailServiceMock = require('../utils/mocks/email');

describe('routes - email', function(){
    const route = proxyquire('../routes/email',{
        '../services/Email': EmailServiceMock
    });
    const request = testServer(route);
    describe('POST /', function () {
        it('should response 200', function (done) {
            request.get('/api/email/').expect(200, done);
        });
    });
});