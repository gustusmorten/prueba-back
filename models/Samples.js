
module.exports = (connection, type) =>{
    return connection.define('sample',{
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dna_object: {
            type: type.STRING,
            allowNull: false
        },
        has_mutation: {
            type: type.BOOLEAN,
            allowNull: false}
    })
}