const sequelize = require('sequelize');
const sampleModel = require('./models/Samples');
const connection = new sequelize(
    process.env.DB_NAME || 'node',
    process.env.DB_USERNAME || 'root',
    process.env.DB_PASSWORD || '',
    {
        host: process.env.DB_HOST || 'localhost',
        dialect: process.env.DB_DRIVER || 'mysql'
    }
)

const sample = sampleModel(connection, sequelize);

connection.sync({force:false})
    .then(()=>{
        console.log('Connection ready.');
    })
    .catch((error) => console.log(error));

module.exports = {
    sample
}