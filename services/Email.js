const nodemailer = require('nodemailer');
const Email = require('email-templates');
var transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASSWORD
  }
});

const emailSender = new Email({
    transport: transport,
    send: true,
    preview: false,
    root: '../utils/emails',
});

class EmailService {
    async sendEmailTo(email) {
        await emailSender.send({
            template: 'welcome',
            message: {
              from: process.env.MAIL_FROM_NAME,
              to: email,
            },
            locals: {},
          }).then(() => console.log(`email has been sent! ${email}`))
          .catch((error) =>  {throw new Error(error)} );
    }
}

module.exports = EmailService;