const expresion = "(\\w)\\1{3,}";
const patt = new RegExp(expresion);
const verifyExpresion = "[^aAtTcCgG]";
const verifyPatt = new RegExp(verifyExpresion);
const {
    sample
} = require('../db');
class MutationsService {

    dnaToVertical(dna) {
        var newDna = [];
        for (let index = 0; index < dna.length; index++) {
            const element = dna[index];
            for (let subIndex = 0; subIndex < element.length; subIndex++) {
                var char = element.charAt(subIndex);
                newDna[subIndex] = newDna[subIndex] ? newDna[subIndex] + char : char;
            }
        }
        return newDna;
    }

    dnaToDiagonal(dna, reverse = false) {
        var newDna = [];
        const firstElement = dna[0];
        const width = dna[0].length
        const heigth = dna.length
        if (reverse) {
            console.log('reverse');

            for (let k = 0; k <= (width + heigth - 2); k++) {
                var newElement = "";
                for (let j = k; j >= 0; j--) {
                    var i = k - j;
                    if (i < heigth && j < width) {
                        var char = dna[i] ? (dna[i][j] ? dna[i][j] : '') : '';
                        newElement = newElement ? newElement + char : char;
                    }
                }
                newDna.push(newElement);
            }
        } else {
            for (let k = 0; k <= (width + heigth - 2); k++) {
                var newElement = "";

                for (let j = 0; j <= k; j++) {
                    var i = k - j;
                    if (i < heigth && j < width) {
                        var char = dna[i] ? (dna[i][j] ? dna[i][j] : '') : '';
                        newElement = newElement ? newElement + char : char;
                    }
                }
                newDna.push(newElement);
            }
        }
        return newDna;
    }

    checkForMutation(dna) {
        var countCoincidences = 0;
        for (let index = 0; index < dna.length; index++) {
            const element = dna[index];
            var res = patt.test(element);
            if (res) {
                countCoincidences++;
            }
        }
        return countCoincidences;
    }
    async storeDNA(res,dna) {
        var lastDna = await sample.findOne({
            where: {
                dna_object: JSON.stringify({
                    dna: dna
                })
            }
        })
        if (!lastDna) {
            await sample.create({
                dna_object: JSON.stringify({
                    dna: dna
                }),
                has_mutation: res
            });
        }
    }

    async verifyDNA(dna){
        for (let index = 0; index < dna.length; index++) {
            const element = dna[index];
            var res = verifyPatt.test(element);
            if (res) {
                throw new Error('invalid DNA');
            }
        }
        return true;
    }

    async hasMutation(dna) {
        await this.verifyDNA(dna);

        var coincidences = this.checkForMutation(dna);
        var verticalDna = this.dnaToVertical(dna);
        coincidences = coincidences + this.checkForMutation(verticalDna);
        var diagonalDna = this.dnaToDiagonal(dna);
        coincidences = coincidences + this.checkForMutation(diagonalDna);
        var diagonalReverseDna = this.dnaToDiagonal(dna, true);
        coincidences = coincidences + this.checkForMutation(diagonalReverseDna);
        var res = coincidences >= 2 ? true : false;
        await this.storeDNA(res, dna);
        return res;

    }
    async stats() {
        const dnaWithMutations = await sample.findAndCountAll({
            where: {
                has_mutation: 1
            },
        });

        const dnaWithNotMutations = await sample.findAndCountAll({
            where: {
                has_mutation: 0
            },
        });
        return {
            count_mutations:dnaWithMutations.count, 
            count_no_mutation:dnaWithNotMutations.count,
            ratio: dnaWithMutations.count / (dnaWithNotMutations.count+dnaWithMutations.count)  
        }
    }
}

module.exports = MutationsService;