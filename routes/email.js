const express = require('express');
const EmailService = require('../services/Email');
function emailAPI(app){
    const router = express.Router();
    app.use("/api/email", router);

    const emailService = new EmailService();
    router.post("/", async function (req,res, next) {
        const { email } = req.body;
        try {
            var e =  await emailService.sendEmailTo(email);
            res.status(200).json({
                data: email,
                message: `email has been sent`
            });
        } catch (error) {
            res.status(500).json({
                data: {},
                message: error.message
            });
            next(error);
        }
    });
}

module.exports  = emailAPI;