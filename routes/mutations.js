const express = require('express');
const MutationsService = require('../services/Mutations');

function mutationAPI(app) {
    const router = express.Router();
    app.use("/api/mutation", router);

    const mutationsService = new MutationsService();
    router.post("/", async function (req, res, next) {
        const {
            dna,
            peps
        } = req.body;
        try {
            const hasMutation = await mutationsService.hasMutation(dna);
            const code = hasMutation ? 200 : 403;
            res.status(code).json({
                data: {
                    hasMutation: hasMutation
                },
                message: ``
            });
        } catch (error) {
            res.status(500).json({
                data: {},
                message: error.message
            });
            next(error);
        }
    });


}

function mutationStatsAPI(app) {
    const router = express.Router();
    app.use("/api", router);
    const mutationsService = new MutationsService();
    router.get("/stats", async function (req, res, next) {
        try {
            const response = await mutationsService.stats();
            res.json({
                data: response,
                message: ''
            });
        } catch (error) {
            res.status(500).json({
                data: {},
                message: error.message
            });
            next(error);
        }
    });

}

module.exports = {
    mutationAPI,
    mutationStatsAPI
};